package route

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"testing"
)

var HandlerOk = func(w http.ResponseWriter, r *http.Request) {
	//fmt.Fprintf(w, "hello world")
	w.WriteHeader(http.StatusOK)
}

var Filter = func(w http.ResponseWriter, r *http.Request) {
	if r.URL.User == nil || r.URL.User.Username() != "admin" {
		http.Error(w, "", http.StatusUnauthorized)
	}
}

var FilterUser = func(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get(":user")
	if id == "admin" {
		http.Error(w, "", http.StatusUnauthorized)
	}
}

func TestRouteOk(t *testing.T) {
	r, _ := http.NewRequest("GET", "/student/three/LiMing?learn=Chinese", nil)
	w := httptest.NewRecorder()

	handler := new(RouteMux)
	handler.Get("/student/:grade/:name", HandlerOk)
	handler.ServeHTTP(w, r)

	params := r.URL.Query()
	gradeParam := params.Get(":grade")
	nameParam := params.Get(":name")
	learnParam := params.Get("learn")

	if gradeParam != "three" {
		t.Errorf("url param set to [%s]; want [%s]", gradeParam, "three")
	}
	if nameParam != "LiMing" {
		t.Errorf("url param set to [%s]; want [%s]", nameParam, "LiMing")
	}
	if learnParam != "Chinese" {
		t.Errorf("url param set to [%s]; want [%s]", learnParam, "Chinese")
	}
}

func TestNotFound(t *testing.T) {
	r, _ := http.NewRequest("GET", "/teacher/two/LiuYang?teach=English", nil)
	w := httptest.NewRecorder()

	handler := new(RouteMux)
	handler.Get("/student/:grade/:name", HandlerOk)
	handler.ServeHTTP(w, r)

	if w.Code != http.StatusNotFound {
		t.Errorf("Code set to [%v]; want [%v]", w.Code, http.StatusNotFound)
	}
}

func TestStatic(t *testing.T) {
	r, _ := http.NewRequest("GET", "/routes_test.go", nil)
	w := httptest.NewRecorder()
	pwd, _ := os.Getwd()

	handler := new(RouteMux)
	handler.Static("/", pwd)
	handler.ServeHTTP(w, r)

	testFile, _ := ioutil.ReadFile(pwd + "/routes_test.go")
	if w.Body.String() != string(testFile) {
		t.Errorf("handler.Static failed to serve file")
	}
}

func TestFilter(t *testing.T) {
	r, _ := http.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()

	handler := new(RouteMux)
	handler.Get("/", HandlerOk)
	handler.Filter(Filter)
	handler.ServeHTTP(w, r)

	if w.Code != http.StatusUnauthorized {
		t.Errorf("Did not apply Filter. Code set to [%v]; want [%v]", w.Code, http.StatusUnauthorized)
	}

	r, _ = http.NewRequest("GET", "/", nil)
	r.URL.User = url.User("admin")
	w = httptest.NewRecorder()
	handler.ServeHTTP(w, r)

	if w.Code != http.StatusOK {
		t.Errorf("Code set to [%v]; want [%v]", w.Code, http.StatusOK)
	}
}

func TestFilterParam(t *testing.T){
	r, _  := http.NewRequest("GET", "/:user", nil)
	w := httptest.NewRecorder()

	handler := new(RouteMux)
	handler.Get("/:user", HandlerOk)
	handler.FilterParam(":user", FilterUser)
	handler.ServeHTTP(w, r)

	if w.Code != http.StatusOK {
		t.Errorf("Did not apply Filter. Code set to [%v]; want [%v]", w.Code, http.StatusOK)
	}

	r, _ = http.NewRequest("GET", "/admin", nil)
	w = httptest.NewRecorder()
	handler.ServeHTTP(w, r)

	if w.Code != http.StatusUnauthorized {
		t.Errorf("Did not apply Param Filter. Code set to [%v]; want [%v]", w.Code, http.StatusUnauthorized)
	}
}

func BenchmarkRoutedHandler(b *testing.B) {
	handler := new(RouteMux)
	handler.Get("/", HandlerOk)

	for i := 0; i < b.N; i++ {
		r, _ := http.NewRequest("GET", "/", nil)
		w := httptest.NewRecorder()
		handler.ServeHTTP(w, r)
	}
}

func BenchmarkServeMux(b *testing.B) {
	r, _ := http.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	mux := http.NewServeMux()
	mux.HandleFunc("/", HandlerOk)

	for i := 0; i < b.N; i++ {
		mux.ServeHTTP(w, r)
	}
}